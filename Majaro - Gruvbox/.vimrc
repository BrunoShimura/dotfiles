set number					"numero de linhas
set ts=4					"tamanho de tab
set ai aw ic scs cul		"auto identação, salvamento automático, destaca a linha
set columns=105				"largura dajanela
set is hls ic scs  			"opções espertas de busca
set sm            	 		"ShowMatch: mostra o início do bloco recém fechado
set sw=1           			"ShiftWidth: número de colunas para o comando >
set ruler          			"régua: mostra a posição do cursor
set shm=filmnrwxt	  		"SHortMessages: encurta as mensagem da régua
set wildmode=longest,list 	"para completação do TAB igual ao bash
set cc=80	   	            " Set rule to 80 columns
syntax on

"guarda a posição de TODOS os arquivos que você editou.
set viminfo='10,\"30,:20,%,n~/.viminfo
au BufReadPost * if line("'\"")|execute("normal `\"")|endif

"salvar W w
cab W  w
cab Wq wq
cab wQ wq
cab WQ wq
cab Q  q

call plug#begin('~/.vim/plugged')
	Plug 'morhetz/gruvbox'
	Plug 'vim-airline/vim-airline'
	Plug 'ap/vim-css-color'
	Plug 'vim-airline/vim-airline-themes'
	Plug 'ryanoasis/vim-devicons'
	Plug 'scrooloose/nerdtree'	
call plug#end()

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
set encoding=UTF-8

" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '►'
let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize=38

" thema do vim
set background=dark
colorscheme gruvbox
let g:gruvbox_contrast_dark = 'hard'

" marcar palavras erradas
highlight SpellBad ctermfg=Gray ctermbg=DarkRed
