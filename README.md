# Dotfiles

## Manjaro i3wm

- ### [Configuração do .vimrc e termite com tema gruvbox](https://gitlab.com/BrunoAnthonyShimura/dotfiles/-/tree/master/Majaro%20-%20Gruvbox)
-------------------------------------------------------------------------------
![vim](https://gitlab.com/BrunoAnthonyShimura/dotfiles/-/wikis/uploads/00a9ff848c4f2431279c15f1b87f4cc9/2020-04-24-093630_1920x1080_scrot.png)

- ### Polybar e Wallpaper
-------------------------------------------------------------------------------
![home](https://gitlab.com/BrunoAnthonyShimura/dotfiles/-/wikis/uploads/78843f22b41a84ae08b15941b68c954a/2020-04-24-093349_1920x1080_scrot.png)

## Arch Linux i3wm

- ### [Configuração do .vimrc e termite com tema dracula](https://gitlab.com/BrunoAnthonyShimura/dotfiles/-/tree/master/Arch%20-%20Dracula)
-------------------------------------------------------------------------------
![vim(arch)](https://gitlab.com/BrunoAnthonyShimura/dotfiles/-/wikis/uploads/8964601db17196578d13c3f289580fd6/1588160040_0834_29.04.2020_1920x1080.png)

- ### Polybar e Wallpaper
-------------------------------------------------------------------------------
![home(arch)](https://gitlab.com/BrunoAnthonyShimura/dotfiles/-/wikis/uploads/d9963bdcef2c4983054e92a594937008/1588160123_0835_29.04.2020_1920x1080.png)
